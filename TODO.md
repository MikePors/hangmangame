- Incorporate some way of auditing the generated data for the quickcheck tests
- Find a way to ensure that the quickcheck tests are each running 100 test cases
-- This may be possible with a custom Spec Reporter
- Separate Tests into different files
- Implement Game Updates:
-- Revealed parts of the secret don't become hidden again
-- Losing

- Improve test performance
-- update: do nothing test

- Setup Halogen as the front-end technology
-- ~Look into random initial state.~ This is done by passing the random initial state into the component during load
-- Look into timers
-- Look into animations

- Tests
-- Find a good way to improve the way test failures are shown to the user

- Setup build step to automate the creation of the js file.
-- Possibly rename the js file from app.js to something else
-- Leaving the app.js file in the output directory may cause problems... be on the lookout
