module Test.Main where

import Prelude

import Effect (Effect)
import Game.HangmanTest (hangmanTests)

main :: Effect Unit
main = do
  hangmanTests
