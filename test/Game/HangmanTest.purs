module Game.HangmanTest where

import Data.Array (catMaybes, foldr, intercalate, singleton)
import Data.Array (null) as Array
import Data.Array.NonEmpty (filter)
import Data.Char (toCharCode)
import Data.DateTime.Instant (unInstant)
import Data.Enum (toEnum)
import Data.Foldable (elem)
import Data.Maybe (Maybe(..), fromJust, maybe)
import Data.Monoid (mempty)
import Data.NonEmpty (NonEmpty(..))
import Data.Semigroup (append, (<>))
import Data.Set (Set, delete, difference, fromFoldable, insert, member, union)
import Data.Set (singleton, fromFoldable, map) as Set
import Data.Show (show)
import Data.String (null)
import Data.String.CodeUnits (fromCharArray, toCharArray, uncons)
import Data.String.CodeUnits (singleton) as CodeUnits
import Data.String.CodeUnits as String
import Data.Time.Duration (negateDuration)
import Effect (Effect)
import Effect.Class.Console (log)
import Effect.Now (now)
import Game.Game (Update(..))
import Game.Hangman (GameState(..), Prisoner(..), depth, inString, isWon, update, withDepth, withGuess)
import Partial.Unsafe (unsafePartial, unsafePartialBecause)
import Prelude (class Eq, class Monad, class Ord, class Show, Unit, bind, discard, flip, map, negate, not, pure, unit, ($), (&&), (-), (/=), (<<<), (==), (>), (>>>))
import Test.QuickCheck (class Arbitrary, Result(..), arbitrary, assertEquals, withHelp)
import Test.QuickCheck.Gen (Gen, chooseInt, elements, listOf, resize, suchThat)
import Test.Spec (describe, describeOnly, it)
import Test.Spec.Assertions (shouldEqual)
import Test.Spec.QuickCheck (quickCheck, quickCheck')
import Test.Spec.Reporter (specReporter)
import Test.Spec.Runner (run)

hangmanTests :: Effect Unit
hangmanTests = do
  run [specReporter] $ do
    describe "depth" $ do
      it "is 0 for a hanged prisoner" $
        depth Hanged `shouldEqual` 0
      it "is 1 for a prisoner one step away from being hanged" $
        depth (Step Hanged) `shouldEqual` 1
      it "is 2 for a prisoner 2 steps away from the noose" $
        depth (Step $ Step Hanged) `shouldEqual` 2
      it "is 3 for a prisoner 3 steps away from the noose" $
        depth (Step $ Step $ Step Hanged) `shouldEqual` 3

    describe "isWon" $ do
      it "returns false if the game is ongoing" $ do
        isWon (GameState { secret: "abc", guesses: Set.singleton 'd', prisoner: Step Hanged }) `shouldEqual` false
        quickCheck $ do
          partialSecret <- resize 20 arbitrary `suchThat` not null
          extraChar <- arbitrary `suchThat` (_ `not member` (fromFoldable $ toCharArray partialSecret))
          let secret = (CodeUnits.singleton extraChar) <> partialSecret
          gameState <- gameStateGenWith $ secretOf secret <<< flip withoutGuess extraChar <<< prisonerWhoIs alive
          assertFalse $ isWon gameState
      it "returns false if the game has been lost" $
        quickCheck $ do
          gameState <- gameStateGenWith $ prisonerWhoIs hanged
          isWon gameState `assertEqualsM` false
      it "returns true if all of the characters in the secret have been guessed" $ do
        isWon (GameState { secret: "abc", guesses: Set.fromFoldable ['a','b','c'], prisoner: Step $ Step Hanged}) `shouldEqual` true
        quickCheck $ do
          secret <- resize 20 arbitrary `suchThat` not null
          gameState <- gameStateGenWith $ secretOf secret <<< (guesses $ fromFoldable $ toCharArray secret)
          isWon gameState `assertEqualsM` true


    describe "update" $ do
      it "changes nothing when the user selects an already selected character" $
        quickCheck update_selectsAlreadyUsedCharacter_noChange
      it "moves prisoner closer to the noose when the user selects a new character which isn't in the secret" $
        quickCheck update_selectsCharacterNotInSecret_prisonerMovedCloserToNoose
      it "adds a missed guess to the collection of missed guesses" $
        quickCheck $ do
          secret <- (resize 20 arbitrary) `suchThat` (not null)
          char <- arbitrary `suchThat` ( _ `not inString` secret)
          gameState <- gameStateGenWith $ (secretOf secret) <<< prisonerWhoIs alive <<< (_ `withoutGuess` char)
          let (Update {display: _, gameState : (GameState record)}) = (update char gameState)
          assertTrue $ char `member` record.guesses
      it "is idempotent (on the gamestate component of its return value)" $
        quickCheck $ do
          secret <- (resize 20 arbitrary) `suchThat` (not null)
          gameState <- gameStateGenWith ((secretOf secret) <<< prisonerWhoIs alive)
          char <- arbitrary
          let (Update { gameState: firstUpdate}) = update char gameState
              (Update {gameState: secondUpdate}) = update char firstUpdate
          assertEqualsM firstUpdate secondUpdate
      it "reveals matching characters when the user guesses a character from the secret" $ do
        quickCheck $ do
          let currentGuesses = NonEmpty 'ʉ' [ 'ॢ' , '⌔' , '♴' , '㤑' , '䐗' , '农' , '掍' , '矻' , '稶' , '膟' , 'ꇖ' , '겾' , '땵' , '�' , '' , ''
          , '理'
          , '﵁' , 'ﾫ']
              specialCase = GameState $ { guesses: (fromFoldable currentGuesses)
                                        , prisoner: Step $ Step Hanged
                                        , secret: "⑰𳭺ᛮ졜帢⾶ᤌ帛�Ὣ髽㐏몆孞㾷"
                                        }
          guess <- elements $ NonEmpty '⑰' (toCharArray "𳭺ᛮ졜帢⾶ᤌ帛�Ὣ髽㐏몆孞㾷")
          let (GameState {guesses: guessesPassedToUpdate}) = withoutGuess specialCase guess
              expectedGuesses = guess `insert` guessesPassedToUpdate
              (Update {gameState: gameState@(GameState record)}) = update guess (withoutGuess specialCase guess)
          pure $ (gameState == (specialCase `flip withGuess` guess) && guess `inString` "⑰𳭺ᛮ졜帢⾶ᤌ帛�Ὣ髽㐏몆孞㾷") `withHelp` ("Guess is: " <> (show $ toCharCode guess) <>
            ".\n GameState is: " <> (show gameState)
            <> ".\n Expected: " <> (show specialCase)
            <> ".\n currentGuesses: " <> (intercalate "," $ map (toCharCode >>> show) currentGuesses)
            <> ".\n A Guesses as int codes: " <> "{" <> (intercalate "," $ Set.map (toCharCode >>> show) record.guesses) <> "}"
            <> ".\n E Guesses as int codes: " <> "{" <> (intercalate "," $ Set.map (toCharCode >>> show) expectedGuesses) <> "}"
            <> ".\n A Secret as int codes: " <> "[" <> (intercalate "," $ map (toCharCode >>> show) $ toCharArray record.secret) <> "]"
            <> ".\n E Secret as int codes: " <> "[" <> (intercalate "," $ map (toCharCode >>> show) $ toCharArray "⑰𳭺ᛮ졜帢⾶ᤌ帛�Ὣ髽㐏몆孞㾷") <> "]"
            <> ".\n Secrets match: " <> (show $ "⑰𳭺ᛮ졜帢⾶ᤌ帛�Ὣ髽㐏몆孞㾷" == record.secret)
            <> ".\n Guesses match: " <> (show $ expectedGuesses == record.guesses)
            <> ".\n Guesses in actual but not in expected: " <> (intercalate "," $ Set.map (toCharCode >>> show) (record.guesses `difference` expectedGuesses))
            <> ".\n Guesses in expected but not in actual: " <> (intercalate "," $ Set.map (toCharCode >>> show) (expectedGuesses `difference` record.guesses))
            <> ".\n Prisoners match: " <> (show $ (Step $ Step Hanged) == record.prisoner)
            <> ".\n Is actual prisoner one step closer to death: " <> (show $ record.prisoner == Step Hanged)
            <> ".\n Guess is in secret: " <> (show $ guess `inString` "⑰𳭺ᛮ졜帢⾶ᤌ帛�Ὣ髽㐏몆孞㾷")
            )
        quickCheck $ do
          secret <- (resize 20 arbitrary) `suchThat` (not null)
          let {head: firstChar, tail: rest} = unsafePartial fromJust $ uncons secret
          char <- elements $ NonEmpty firstChar (toCharArray rest)
          initialGameState@(GameState initialRecord) <- gameStateGenWith $ (secretOf secret) <<< prisonerWhoIs alive <<< (_ `withoutGuess` char)
          let (Update {gameState: updatedGameState@(GameState record)}) = update char initialGameState
              expectedGuesses = insert char initialRecord.guesses
              expectedGameState = GameState $ initialRecord {guesses = expectedGuesses}
          assertEqualsM expectedGameState updatedGameState

      it "ends the game as a loss when the prisoner is hanged" $
        quickCheck $ do
          secret <- (resize 20 arbitrary) `suchThat` (not null)
          guess <- arbitrary `suchThat` (_ `not inString` secret)
          gameState <- gameStateGenWith $ secretOf secret <<< prisonerWhoIs (Step <<< hanged) <<< (_ `withoutGuess` guess)
          let (Update { display: outcome }) = update guess gameState
          assertEqualsM "You lost!" outcome
      it "does not modifiy the game state if the user has already lost" $
        quickCheck $ do
          gameState <- gameStateGenWith $ prisonerWhoIs hanged
          guess <- arbitrary
          let (Update updatedRecord) = update guess gameState
          pure $ results $ [ assertEquals "The game is over.  You have lost." updatedRecord.display
                           , assertEquals gameState updatedRecord.gameState
                           ]
      it "ends the game as a loss even if the user finishes the secret after the prisoner is dead" $
        let gameState = GameState { secret: "abc", prisoner: Hanged, guesses: fromFoldable ['a', 'b']}
            (Update {display: outcome }) = update 'c' gameState
        in outcome `shouldEqual` "The game is over.  You have lost."
      it "will not modify  a game which is in a winning state" $
        quickCheck $ do
          secret <- resize 20 arbitrary `suchThat` (not null)
          gameState <- gameStateGenWith $ secretOf secret <<< prisonerWhoIs alive <<< (guesses $ fromFoldable $ toCharArray secret)
          guess <- arbitrary
          let (Update actual) = update guess gameState
          pure $ results $ [ assertEquals "The game is over.  You have won." actual.display
                          , assertEquals gameState actual.gameState
                           ]
      it "ends the game as a win when the player guesses the last character of the secret" $
        let gameState = GameState { secret: "abc", guesses: fromFoldable ['a', 'b'], prisoner: Step Hanged }
            (Update {display: actualDisplay, gameState: actualGameState }) = update 'c' gameState
        in do
          actualDisplay `shouldEqual` "Congratulations!  You've won!"
          actualGameState `shouldEqual` (withGuess 'c' gameState)
      it "ends game as a win when the player guesses the last character of the secret (that character is repeated)" $
        let gameState = GameState { secret: "abcc", guesses: fromFoldable ['a', 'b'], prisoner: Step $ Step Hanged }
            (Update {display: actualDisplay, gameState: actualGameState }) = update 'c' gameState
        in do
          actualDisplay `shouldEqual` "Congratulations!  You've won!"
          actualGameState `shouldEqual` (withGuess 'c' gameState)

    describe "withDepth >>> depth" $ do
      it "acts as an inverse on positive integers" $
        quickCheck withDepth_depth_inverseOnPositiveInts
      it "returns 0 on negative integers" $
        quickCheck withDepth_depth_returnsZeroOnNegativeInts

    describe "depth >>> withDepth" $
      it "is the identity function on Prisoners" $
        quickCheck depth_withDepth_inverse

    describe "inString" $ do
      it "returns true when a given character is in the given string" $ do
        quickCheck' 10 inString_shouldReturnTrue
        quickCheck $ do
          firstChar <- arbitrary `suchThat` (toCharCode >>> (_ > 55664))
          secretTail <- resize 20 arbitrary
          let secret = (String.singleton firstChar) <> secretTail
          char <- elements $ NonEmpty firstChar $ toCharArray secretTail
          pure $ inString firstChar secret `withHelp` ("Guess (as int code): " <> (show $ toCharCode char)
                                                      <> ",\nGuess (as character): " <> (show char)
                                                      <> ",\nSecret (as int codes): " <> (show $ map toCharCode $ toCharArray secret)
                                                      <> ",\nSecret (as string): " <> secret)

      it "returns false when the given character is not in the given string" $ do
        quickCheck' 10 inString_shouldReturnFalse
        quickCheck $ do
          secret <- resize 20 arbitrary
          let secretAsCharArray = toCharArray secret
          char <- arbitrary `suchThat` (_`not elem` secretAsCharArray)
          assertFalse $ inString char secret

{-
Sample usage:
  timedTest "Already used character" (\u -> quickCheckGen update_selectsAlreadyUsedCharacter_noChange)
  timedTest "Prisoner moves closer to noose" (\u -> quickCheckGen update_selectsCharacterNotInSecret_prisonerMovedCloserToNoose)
-}
results :: Array Result -> Result
results rs =
  let errorMessages = catMaybes $ resultAsMaybeError `map` rs
  in if Array.null errorMessages
  then Success
  else Failed $ "Failure.\n" <> intercalate ",\n " errorMessages

resultAsMaybeError :: Result -> Maybe String
resultAsMaybeError result =
  case result of
    Success      -> Nothing
    (Failed msg) -> Just msg

timedTest :: String -> (Unit -> Effect Unit) -> Effect Unit
timedTest label test = do
  log $ "Start timing for: " <> label
  start <- now
  test unit
  end <- now
  let testTime = (unInstant end) <> (negateDuration $ unInstant start)
  log $ "End timing. Took: " <> (show testTime)

withDepth_depth_inverseOnPositiveInts :: Gen Result
withDepth_depth_inverseOnPositiveInts = do
  n <- chooseInt 0 100
  let prisoner = withDepth n
      prisonerDepth = depth prisoner
  assertTrue $ prisonerDepth == n

withDepth_depth_returnsZeroOnNegativeInts :: Gen Result
withDepth_depth_returnsZeroOnNegativeInts = do
  n <- chooseInt (negate 100) 0
  let prisoner = withDepth n
      prisonerDepth = depth prisoner
  assertTrue $ prisonerDepth == 0

depth_withDepth_inverse :: Gen Result
depth_withDepth_inverse = do
  n <- chooseInt 0 100
  let prisoner = withDepth n
      prisonerDepth = depth prisoner
      newPrisoner = withDepth prisonerDepth
  assertTrue $ prisoner == newPrisoner

inString_shouldReturnTrue :: Gen Result
inString_shouldReturnTrue = do
  let testString = "abcdefghigedba"
      legalChars = NonEmpty ('a') $ (toCharArray ("bcdefghi"))
  char <- elements legalChars
  pure $ (char `inString` testString) `withHelp` ((show char) <> " is in " <> testString <> " but inString returned 'false'")

inString_shouldReturnFalse :: Gen Result
inString_shouldReturnFalse = do
  let testString = "mnopqrstuvwxyz"
      legalChars = NonEmpty 'a' $ (toCharArray "bcdefghijkl")
  char <- elements legalChars
  pure $  (char `not <<< inString` testString) `withHelp` ((show char) <> " is not in " <> testString <> " but inString returned 'true'")

update_selectsCharacterNotInSecret_prisonerMovedCloserToNoose :: Gen Result
update_selectsCharacterNotInSecret_prisonerMovedCloserToNoose = do
  secret <- (resize 20 arbitrary) `suchThat` (not null)
  let {head: firstChar, tail: restAsString} = unsafePartialBecause "the not null check above guarantees non-emptiness" $ fromJust $ uncons secret
      rest = toCharArray restAsString
  char <- arbitrary `suchThat` (_ `not inString` secret)
  gameState@(GameState gsRecord) <- gameStateGenWith $ (secretOf secret) <<< prisonerWhoIs alive <<< (_ `withoutGuess` char)
  let (Update {display: _, gameState : (GameState record)}) = (update char gameState)
      newPrisoner = record.prisoner
      expectedDepth = (depth gsRecord.prisoner) - 1
  pure $ (expectedDepth == depth newPrisoner) `withHelp` ("Prisoner should have had depth " <> (show expectedDepth) <> ", but was: " <> (show $ depth newPrisoner))

update_selectsAlreadyUsedCharacter_noChange :: Gen Result
update_selectsAlreadyUsedCharacter_noChange = do
  guess <- arbitrary
  gameState@(GameState gs) <- gameStateGenWith $ (guess `asGuess` _) <<< prisonerWhoIs alive

  let expected = Update {display: "No Change", gameState: gameState}
      actual = update guess gameState
  pure $ (expected == actual) `withHelp` ("GameState changed when it shouldn't have: Old = [" <> (show gameState) <> "], New =[" <> (show actual) <> "]")

withoutGuess :: GameState -> Char -> GameState
withoutGuess gs@(GameState record) guess =
  let newGuesses = delete guess record.guesses
  in GameState $ record { guesses = newGuesses }

guesses :: Set Char -> GameState -> GameState
guesses newGuesses (GameState record) =
  GameState $ record { guesses = newGuesses `union` record.guesses }

asGuess :: Char -> GameState -> GameState
asGuess c = guesses $ Set.singleton c

arbitrarySet :: forall a . (Arbitrary a) => Ord a => Int -> Gen (Set a)
arbitrarySet size = do
    arbitraryList <- listOf size arbitrary
    pure $ fromFoldable arbitraryList


gameStateGen :: Gen GameState
gameStateGen = do
  secret <- (resize 20 arbitrary) `suchThat` (not <<< null)
  depth <- chooseInt 0 100
  let prisoner = withDepth depth
  arbitraryGuesses <- arbitrarySet 20
  pure $ GameState {secret, prisoner, guesses: arbitraryGuesses}

gameStateGenWith :: (GameState -> GameState) -> Gen GameState
gameStateGenWith updater = do
  gameState <- gameStateGen
  pure $ updater gameState

secretOf :: String -> GameState -> GameState
secretOf newSecret (GameState record) = GameState (record {secret = newSecret})

prisonerWhoIs :: (Prisoner -> Prisoner) -> GameState -> GameState
prisonerWhoIs fate (GameState record) = GameState $ record { prisoner = fate record.prisoner }

alive :: Prisoner -> Prisoner
alive Hanged = Step Hanged
alive alivePrisoner = alivePrisoner

hanged :: Prisoner -> Prisoner
hanged _ = Hanged

gameStateGenWithSecret :: String -> Gen GameState
gameStateGenWithSecret secret = gameStateGenWith (\(GameState record) -> GameState (record {secret = secret}))

isPartOfSecret :: Char -> GameState -> Boolean
isPartOfSecret c (GameState record) = c `inString` record.secret

secretIsNonEmpty :: GameState -> Boolean
secretIsNonEmpty (GameState record) = not null record.secret

prisonerWhoIsAlive :: GameState -> Boolean
prisonerWhoIsAlive (GameState record) = record.prisoner /= Hanged

assertTrue :: forall m. Monad m => Boolean -> m Result
assertTrue bool = pure $ assertEquals true bool

assertFalse :: forall m . Monad m => Boolean -> m Result
assertFalse bool = pure $ assertEquals false bool

assertEqualsM :: forall m a. Monad m => Eq a => Show a => a -> a -> m Result
assertEqualsM x y = pure $ assertEquals x y
