module Halogen.Component.Main (component, Query) where

import Data.Maybe (Maybe(..))
import Data.Set (empty)
import Effect (Effect)
import Effect.Aff (Aff)
import Effect.Random (randomInt)
import Game.Hangman (GameState(..), Prisoner(..), depth, withDepth)
import Halogen as H
import Halogen.Component.Hangman.GameState.GameStateViewComponent as GameStateViewComponent
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Prelude (type (~>), Unit, Void, const, pure, show, unit, ($), (<>), bind, discard, class Eq, class Ord)

data Query a = GenerateRandom a

type State = Maybe GameState

data Slot = GameStateView
derive instance eqSlot :: Eq Slot
derive instance ordSlot :: Ord Slot

component :: H.Component HH.HTML Query Unit Void Aff
component =
  H.parentComponent
    { initialState: const initialState
    , render
    , eval
    , receiver: const Nothing
    }
  where

  initialState :: State
  initialState = Nothing

  render :: State -> H.ParentHTML Query GameStateViewComponent.Query Slot Aff
  render state =
    let mainView = case state of
                    Just gameState -> HH.slot GameStateView GameStateViewComponent.component gameState (const Nothing)
                    Nothing       -> HH.p_ [ HH.text $ "Create a random prisoner!"]
        randomize = HH.button [ HE.onClick $ HE.input_ GenerateRandom ] [ HH.text "Generate random prisoner" ]
    in HH.div_ [ mainView, randomize ]

  eval :: Query ~> H.ParentDSL State Query GameStateViewComponent.Query Slot Void Aff
  eval = case _ of
    GenerateRandom a -> do
      newVal <- H.liftEffect $ randomInt 1 20
      let newState = Just $ GameState { secret: "Test", guesses: empty, prisoner: withDepth newVal }
      H.put newState
      pure a
