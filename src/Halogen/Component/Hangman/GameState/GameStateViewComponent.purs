module Halogen.Component.Hangman.GameState.GameStateViewComponent where

import Data.Foldable (intercalate, surround)
import Data.Maybe (Maybe(..))
import Data.Set (Set, empty, member)
import Data.String.CodeUnits (fromCharArray, toCharArray)
import Effect (Effect)
import Effect.Aff (Aff)
import Effect.Random (randomInt)
import Game.Hangman (GameState(..), Prisoner(..), depth, withDepth)
import Halogen (put)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Prelude (type (~>), Unit, Void, bind, const, discard, flip, pure, show, unit, ($), (<<<), (<>), (>>>), map, class Show)

data Query a = NewGameState a GameState

component :: H.Component HH.HTML Query GameState Void Aff
component =
  H.component
    { initialState: (\x -> x)
    , render
    , eval
    , receiver: Just <<< NewGameState unit
    }
  where

    render :: GameState -> H.ComponentHTML Query
    render gameState@(GameState { prisoner }) =
      HH.div_ [ prisonerView prisoner, secretView gameState ]

    eval :: forall f. Query ~> H.ComponentDSL GameState Query Void f
    eval (NewGameState a gameState) = do
      put gameState
      pure a

prisonerView :: forall p i . Prisoner -> HH.HTML p i
prisonerView prisoner =
  let steps = depth prisoner
  in HH.p_ [ HH.text $ "The prisoner is " <> (show steps) <> " steps away from the noose!" ]

data Mask = Hidden | Visible Char

asChar :: Mask -> Char
asChar masked =
  case masked of
    Hidden -> '_'
    (Visible c) -> c

mask :: Set Char -> Char -> Mask
mask guesses toMask =
  if toMask `member` guesses
  then Visible toMask
  else Hidden

maskSecret :: Set Char -> String -> String
maskSecret guesses secret = fromCharArray $ map asChar $ map (mask guesses) $ toCharArray secret

secretView :: forall p i . GameState -> HH.HTML p i
secretView (GameState { secret, guesses }) =
  let maskedSecret = maskSecret guesses secret
  in HH.p_ [ HH.text $ "Secret: " <> maskedSecret ]
