module Game.Hangman where

import Prelude

import Data.Array (dropWhile, null)
import Data.Foldable (all)
import Data.Set (Set, insert, member)
import Data.String.CodeUnits (toCharArray)
import Game.Game (Update(..))

data Prisoner = Hanged | Step Prisoner

derive instance prisonerEq :: Eq Prisoner
instance prisonerShow :: Show Prisoner where
  show prisoner =
    case prisoner of
      Hanged -> "Hanged"
      (Step inner) -> "Step (" <> show inner <> ")"

withDepth :: Int -> Prisoner
withDepth n | n <= 0 = Hanged
            | otherwise = Step $ withDepth (n-1)

depth :: Prisoner -> Int
depth prisoner = countDepth prisoner 0
  where countDepth p n =
          case p of
            Hanged -> n
            (Step p2) -> countDepth p2 (n + 1)


data GameState = GameState { secret :: String, prisoner :: Prisoner, guesses :: Set Char }

derive instance gameStateEq :: Eq GameState
instance gameStateShow :: Show GameState where
  show (GameState record) = "GameState " <> show record

update :: Char -> GameState -> Update String GameState
update guess gameState@(GameState record) | record.prisoner == Hanged = Update { display: "The game is over.  You have lost.", gameState: gameState }
                                          | isWon gameState = Update { display: "The game is over.  You have won.", gameState: gameState }
                                          | guess `member` record.guesses = Update { display: "No Change", gameState: gameState }
                                          | guess `inString` record.secret = let newGameState = withGuess guess gameState
                                                                             in if isWon newGameState
                                                                                then Update { display: "Congratulations!  You've won!", gameState: newGameState }
                                                                                else Update { display: "More of the secret is revealed", gameState: newGameState }
                                          | otherwise = let updatedGameState@(GameState newRecord) = stepTowardsNoose $ withGuess guess gameState
                                                        in Update { gameState: updatedGameState
                                                                  , display : case newRecord.prisoner of
                                                                                (Step _) -> "One step closer to death"
                                                                                Hanged   -> "You lost!"
                                                                  }

withGuess :: Char -> GameState -> GameState
withGuess guess (GameState record) =
  let updatedGuesses = insert guess record.guesses
  in GameState $ record { guesses = updatedGuesses }

isWon :: GameState -> Boolean
isWon (GameState record) = all (_ `member` record.guesses) $ toCharArray record.secret

stepTowardsNoose :: GameState -> GameState
stepTowardsNoose (GameState record) =
  let prisonerNow = case record.prisoner of
                      (Step futurePrisoner) -> futurePrisoner
                      Hanged -> Hanged
  in GameState $ record { prisoner = prisonerNow }

inString :: Char -> String -> Boolean
inString char string = not null $ dropWhile (_ /= char) (toCharArray string)
