module Game.Game where

import Prelude

import Control.Monad.State (StateT, put, get, lift)

data Update display gameState = Update { display :: display, gameState :: gameState }

derive instance updateEq :: (Eq display, Eq gameState) => Eq (Update display gameState)
instance showUpdate :: (Show  display, Show gameState) => Show (Update display gameState)
  where show (Update record) = "Update {display="<> (show record.display)<>", gameState="<>(show record.gameState)<>"}"

runTurn :: forall m display turn gameState . Monad m => m turn -> (turn -> gameState -> Update display gameState) -> StateT gameState m display
runTurn nextMove doUpdate = do
  move <- lift nextMove
  currentGameState <- get
  let (Update update) = doUpdate move currentGameState
  put update.gameState
  pure update.display
